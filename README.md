
## Data visualization app

This small app is intended for mobile devices to demonstrate proficiency with both Ionic and Angular 1.x.

Features:
 * front-end database using PouchDB and SQLite
 * jquery functionality wrapped in directives
 * side-menu navigation
 * uses lodash and localstorage for persistence and transformation
