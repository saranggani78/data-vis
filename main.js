// file main.js

angular.module("MainModule",["ionic"])

    .run(function($ionicPlatform, siteFactory) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
            siteFactory.init();
        });
    })

    .config(function($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('menu/dashboard');

        $stateProvider

            // side menu
            .state('sidemenu',{
                abstract: true,
                url: '/menu',
                templateUrl: 'app/sidemenu/side-menu.html'
            })
            // dashboard for all sites
            .state('sidemenu.dashboard', {
                url: '/dashboard',// will be #/tab/dashboard
                views: {
                    'mainContent': {
                        templateUrl: 'app/dashboard/dashboard.html',
                        controller: 'DashboardCtrl'
                    }
                }
            })
            // sites list
            .state('sidemenu.sites', {
                url: '/sites',
                views: {
                    'mainContent': {
                        templateUrl: 'app/sitelist/site-list.html',
                        controller: 'SiteListCtrl'
                    }
                }
            })
            // site detail
            .state('sidemenu.site-detail', {
                url: '/site/:siteId',
                views: {
                    'mainContent': {
                        templateUrl: 'app/sitelist/site-detail.html',
                        controller: 'SiteDetailCtrl'
                    }
                }
            })
            .state('sidemenu.new-test',{
                url:'/new-test',
                views:{
                    'mainContent':{
                        templateUrl: 'app/testform/new-test.html',
                        controller: 'NewTestCtrl'
                    }
                }
            })
            // p1-p3 are children of the menu form but not of the sidemenu
            .state('sidemenu.t1',{
                url:'/test1',
                views:{
                    'mainContent':{
                        templateUrl: 'app/testform/new-test-p1.html'
                    }
                }
            })
            .state('sidemenu.t2',{
                url:'/test2',
                views:{
                    'mainContent':{
                        templateUrl: 'app/testform/new-test-p2.html'
                    }
                }            })
            .state('sidemenu.t3',{
                url:'/test3',
                views:{
                    'mainContent':{
                        templateUrl: 'app/testform/new-test-p3.html'
                    }
                }            })
            // controls list
            .state('sidemenu.control', {
                url: '/control',
                views: {
                    'mainContent': {
                        templateUrl:"app/control/control-list.html",
                        controller: 'ControllistCtrl'
                    }
                }
            })
            // controls detail
            .state('sidemenu.pattern',{
                url: '/pattern/:controlId',
                views:{
                    'mainContent':{
                        templateUrl: 'app/control/control-pattern.html',
                        controller: 'PatternCtrl'
                    }
                }
            })
        ;

    });
