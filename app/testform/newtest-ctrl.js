/* 
 * conducts data transformation and manipulation
 * update the tests on site controls
 * since the sites and controls stay the same only the test results change
 * TODO connect to mongodb 
 */
'use strict';
angular.module('MainModule')
    .controller('NewTestCtrl', function($scope, siteFactory,$state,$ionicHistory) {
        var vm = this;
        vm.site,vm.date,vm.ac1,vm.bd2,vm.ce3;

        vm.processForm = function(res){
            vm.saveCE(res);
            siteFactory.populateData();
             // console.log('new data: ',JSON.parse(window.localStorage['data']));

            $ionicHistory.nextViewOptions({historyRoot:true}); // reset nav history, disable back button
            $state.go('sidemenu.dashboard');
        };

        // update data with new formData
        vm.saveSite= function(site,date){
            siteFactory.saveSite(site,date);
            vm.print();
        }
        vm.saveAC = function(res){
            siteFactory.saveSiteData('AC-1',res);
            vm.print();
        };
        vm.saveBD = function(res){
            siteFactory.saveSiteData('BD-2',res);
            vm.print();
        };
        vm.saveCE = function(res){
            siteFactory.saveSiteData('CE-3',res);
            vm.print();
        };

        vm.print = function(){
            siteFactory.printFormData();
        };
        return vm;
    });
