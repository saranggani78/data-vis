// dependency siteAPI
'use strict';
angular.module('MainModule')
    .controller('SiteListCtrl',function(){
        var ctrl = this;

        // TODO localStorage maybe error prone when testing in device
        // TODO: http://gonehybrid.com/dont-assume-localstorage-will-always-work-in-your-hybrid-app/
        // TODO http://gonehybrid.com/how-to-use-pouchdb-sqlite-for-local-storage-in-your-ionic-app/
        // TODO http://gonehybrid.com/how-to-use-lokijs-for-local-storage-in-your-ionic-app/
        if(typeof window.localStorage['data'] !== 'undefined'){

        ctrl.data = JSON.parse(window.localStorage['data']);
        console.log('ctrl.data: ', JSON.stringify(ctrl.data, null, 4));
        }
        else{
          console.log('local storage null');
          // ctrl.data = JSON.parse(window.localStorage['data']);
        }


        return ctrl;
    });
