// dependencies: nvd3, siteAPI
// get the city from URL parameters
'use strict';
angular.module('MainModule')
.controller('SiteDetailCtrl',function($stateParams, siteFactory){
    var ctrl = this;
    var siteData = JSON.parse(window.localStorage['data']);
    var datearray = [];
    ctrl.wrapper = [];
    var tempwrapper = [];
    ctrl.controlarray =['AC-1','BD-2','CE-3'];

    for(var i=0;i<siteData.length;i++){
        if(siteData[i].site.name === $stateParams.siteId){
            ctrl.data = siteData[i];
            ctrl.title = siteData[i].site.name;
            var controls = siteData[i].site.control;
            // populate all existing test dates
            for(var j=0;j<controls.length;j++){
                var tests = controls[j].test;
                for(var k=0; k<tests.length;k++){
                    for(var t in tests){
                        datearray.push(tests[t].testDate);
                    }
                }
            }
                console.log('datearray: ', JSON.stringify(datearray, null, 4));
                // debugger;
            // populate formData
            datearray = _.uniq(datearray);
            for(var j=0;j<controls.length;j++) {
                var tests = controls[j].test;
                for(var t in tests){
                    for(var d=0; d<datearray.length;d++) {
                        var formData = {};
                        if(datearray[d] === tests[t].testDate){
                            formData.testDate = tests[t].testDate;
                            if(controls[j].name === 'AC-1'){
                                formData.ac1 = tests[t].result;
                            }
                            if(controls[j].name === 'BD-2'){
                                formData.bd2 = tests[t].result;
                            }
                            if(controls[j].name === 'CE-3'){
                                formData.ce3 = tests[t].result;
                            }
                            tempwrapper.push(formData);
                        }
                    }

                }

            }

        }
    }
    var tempwrapper2 = [];
    var formData = {};
    for(var d=0;d<datearray.length;d++){
        for(var t=0;t<tempwrapper.length;t++){
                formData.testDate = datearray[d];
            if((tempwrapper[t].testDate == datearray[d]) && tempwrapper[t].ac1){
                    formData.ac1 = tempwrapper[t].ac1;
            }
            if((tempwrapper[t].testDate == datearray[d]) &&tempwrapper[t].bd2){
                formData.bd2 = tempwrapper[t].bd2;
            }
            if((tempwrapper[t].testDate == datearray[d]) &&tempwrapper[t].ce3){
                formData.ce3 = tempwrapper[t].ce3;
            }
        }
        tempwrapper2.push(formData);
        formData = {};
    }
    // tempwrapper2 = _.uniq(tempwrapper2);
    _.map(tempwrapper2,function(t){
        t.testDate = new Date(t.testDate).toDateString();
    });
    ctrl.wrapper = tempwrapper2;
    console.log('site detail: ', JSON.stringify(ctrl.data, null, 4));
    console.log('datearray: ', JSON.stringify(datearray, null, 4));
    console.log('tempwrapper: ', JSON.stringify(tempwrapper, null, 4));
    console.log('tempwrapper2: ', JSON.stringify(tempwrapper2, null, 4));

    return ctrl;
});
