// file: dbService.js
// this service is responsible for handling database transactions
// uses PouchDB as db abstraction layer and SQLite for mobile devices
 
angular.module('MainModule')
.factory('dbFactory',['$q', '$http', function($q, $http){
    
    var _db;
    var _data = []; // this array is used by getAllData
    var timestamp = String(new Date().getTime()); // used to control pouchdb doc id
    
    // follows model convention see siteFactory.js
    /*
    var site = {
        "_id" : timestamp, // this can be site name
        "name" : $scope.siteName, // string
        "control" : [{
            "name" : $scope.controlName, // string
            "test" : [{
                "result" : $testResult, // string
                "testDate" : $testDate // ISO Date Time
            }]
        }]
    };
    */
    
    function initDB(){
        _db = new PouchDB('siteDB', {adapter: 'websql'});
        
    };
    
    // params: new site object
    function addData(site){
    
        var temp = site;
        temp._id = site.name
        console.log('%%%%% putting new site: ', JSON.stringify(site));
        _db.put(temp);
    
    };
    
    // returns: array of objects
    function getAllData(){
        
        _db.allDocs({include_docs: true})
        .then(function(result){
            console.log(JSON.stringify(result.rows));
            return result.rows;
        })
        .catch(function(error){console.log(JSON.stringify(error))});
    };
        
    // params: existing site object
    function updateData(site){
        // query _db for id of site
        // handle err
        // site exists, replace with site
    };
    
    // params: id of existing site object
    function deleteData(siteID){
        // query _db for siteID
        // handle err
        // site exists, delete site
        // TODO separate controls or tests to normalize database
    };
    
    return {
        initDB : initDB,
        getAllData : getAllData,
        addData : addData,
        updateData : updateData,
        deleteData : deleteData
    }

}]);
