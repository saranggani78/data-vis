//
// business logic that is shared by all controllers
'use strict';
angular.module('MainModule')
.factory('siteFactory',['$http','dbFactory', function($http, dbFactory){
        var site,date,ac1,bd2,ce3;
        var formData = {site:'',date:'',ac1:'',bd2:'',ce3:''};

        var init = function(){
          // initialize data here for testing

          var promise = $http.get("./app/services/data.json").then(function(response){

              console.log('init localstorage');
              window.localStorage['data'] = JSON.stringify(response.data);
              return response.data;
            }
          );
          return promise;

        }
        // push formData into siteData
        var saveSiteData = function(control, result){
            //populating formData for logging to console
            console.log(formData.site);
            if(control === 'AC-1'){formData.ac1=result};
            if(control === 'BD-2'){formData.bd2=result};
            if(control === 'CE-3'){formData.ce3=result};


        }
        //  update changes to main data set added by form wizard, used by dashboard and site details
        var populateData = function(){
            var siteData = JSON.parse(window.localStorage['data']);
            for(var i=0;i<siteData.length;i++) {
               console.log(i,siteData[i].site.name);
                if(siteData[i].site.name === this.site){
                    // console.log('controls: ', JSON.stringify(siteData[i].site.control, null,4));
                    for(var j=0;j<siteData[i].site.control.length;j++){
                        if(siteData[i].site.control[j].name === 'AC-1'){
                            // new test
                            var newObj = {}
                            newObj.result = formData.ac1;
                            newObj.testDate = this.date;
                            siteData[i].site.control[j].test.push(newObj);
                        }
                        if(siteData[i].site.control[j].name === 'BD-2'){
                            var newObj = {}
                            newObj.result = formData.bd2;
                            newObj.testDate = this.date;
                            siteData[i].site.control[j].test.push(newObj);
                        }
                        if(siteData[i].site.control[j].name === 'CE-3'){
                            var newObj = {}
                            newObj.result = formData.ce3;
                            newObj.testDate = this.date;
                            siteData[i].site.control[j].test.push(newObj);
                        }
                    }
                    console.log('new controls: ',JSON.stringify(siteData[i].site.control, null,4));
                }
            }
            // window.localStorage.setItem('data', JSON.stringify(siteData));
            window.localStorage['data'] = JSON.stringify(siteData);
        }
        var saveSite = function(site,date){
            this.site = site;
            this.date = date;
            formData.site=this.site;
            formData.date=this.date;
        }

        var printFormData = function(){
            console.log('form data: ', JSON.stringify(formData,null,4));
        }
        // TODO using $resource to get all data from RESTful endpoint
        // use dbFactory to get all docs from pouchDB
        var getFromRemote = function(){
            // use localStorage for now
        }
        // TODO save test results to endpoint
        // commit changes with pouchDB change API
        var saveToRemote = function(siteData){
        }

        return{
            saveSiteData : saveSiteData,
            saveSite : saveSite,
            printFormData : printFormData,
            populateData : populateData,
            init : init
        };

}]);
