/*
 this is the entry point as defined by the routes
 TODO nvd3 pie chart graph that covers all sites
 TODO nvd3 bar graph to cover history of progress
 */

'use strict';
angular.module('MainModule')
    .controller('DashboardCtrl', function( $scope, countService,siteFactory, $http){
        var ctrl = this;
        

        // get the average count of compliants
        ctrl.getCurrentAvg = function(){
          countService.getAverage();
        };
        //  load the data into local storage for manipulation at runtime
        ctrl.getSiteData = function(){
          
          window.localStorage.clear();
          siteFactory.init().then(function(data){
            ctrl.data = JSON.parse(window.localStorage['data']);
            console.log('ctrl.data: ', JSON.stringify(ctrl.data, null, 4));
          });
          
        };

        return ctrl;


    });
